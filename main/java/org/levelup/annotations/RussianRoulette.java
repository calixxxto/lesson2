package org.levelup.annotations;

public class RussianRoulette {

    @RandomInt(min = 1, max = 6)
    public int number;

    private double withoutAnnotation;

    public void guess(int number){
        if (number == this.number) {
            System.out.println("you are died...");
        }
    }

    public int getNumber() {
        return number;
    }
}
