package org.levelup.annotations;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class RandomIntAnnotationProcessor {

    public static void setField(Object object) throws IllegalAccessException, AnnotationTypeException {

        Class<?> objectClass = object.getClass();
        Field[] field = objectClass.getDeclaredFields();

        for (Field field1 : field) {
            System.out.println("Field: " + field1.getName() + "Field type: " + field1.getType());

            // homework 1
            if(!field1.getType().isAssignableFrom(Integer.TYPE) || !field1.getType().isAssignableFrom(int.class)) {
                throw new AnnotationTypeException("Тип данных " + field1.getType() + " НЕ соответствует int или Integer");
            } else {
                System.out.println("Тип данных НОРМ : " + field1.getType());
            }

            RandomInt annotation = field1.getAnnotation(RandomInt.class);
            System.out.println("Is null: " + (annotation == null));

            if (annotation != null) {
                int number = new Random()
                        .nextInt(annotation.max() - annotation.min() + 1) + annotation.min();
                System.out.println(number);
                field1.setAccessible(true);
                field1.set(object, number);
            }
        }

    }
}
