package org.levelup.annotations;

public class AnnotationTypeException extends Exception {

    private int number;

    public AnnotationTypeException(String message){
        super(message);
    }

    public int getNumber() {
        return number;
    }
}
